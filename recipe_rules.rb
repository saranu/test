#
# Foodcritic:: StandardPractices
# RuleSet:: Recipe
#
# maintainer:: Opttech Corp, LLC
# maintainer_email:: devops@opttechcorp.com
#
# Copyright:: 2017, Opttech Corp, All Rights Reserved.


## Password declaration check in recipe files.(tag: correctness)

rule 'AIG_RFC001', 'Password declaration check in recipe files' do
  tags %w{bug cink}
  cookbook do |path|
    recipes  = Dir["#{path}/{#{standard_cookbook_subdirs.join(',')}}/**/*.rb"]
    recipes += Dir["#{path}/*.rb"]
    recipes.collect do |recipe|
      lines = File.readlines(recipe)

      lines.collect.with_index do |line, index|
        if line.match('(.*)(password)(.*)')
          {
            :filename => recipe,
            :matched => recipe,
            :line => index+1,
            :column => 0
          }
        end
      end.compact
    end.flatten
  end
end



## Resource check with no action. missing default action check.(tag: correctness)
@coreservices = ["package", "remote_file", "service", "file"]
rule "AIG_RFC002", "missing default action" do
  tags %w{style recipe}
  recipe do |ast, filename|
    find_resources(ast).select do |resource|
      notifications(resource).any? do |notification|
        @coreservices.include?(notification[:resource_name]) and
          notification[:action].nil?
      end
    end
  end
end


## Execute resource defined without conditional or action :nothing(tag: correctness)

rule "AIG_RFC003", "Execute resource defined without conditional or action :nothing" do
 tags %w{style recipe}
 recipe do |ast,filename|
   pres = find_resources(ast, :type => 'execute').find_all do |cmd|
     cmd_actions = (resource_attribute(cmd, 'action') || resource_name(cmd)).to_s
     condition = Nokogiri::XML(cmd.to_xml).xpath('//ident[@value="only_if" or @value="not_if" or @value="creates"][parent::fcall or parent::command or ancestor::if]')
     (condition.empty? && !cmd_actions.include?("nothing"))
   end.map{|cmd| match(cmd)}
 end
end


## bash resource defined without conditional or action :nothing(tag: correctness)

rule "AIG_RFC004", "bash resource defined without conditional or action :nothing" do
 tags %w{style recipe etsy}
 recipe do |ast,filename|
   pres = find_resources(ast, :type => 'bash').find_all do |cmd|
     cmd_actions = (resource_attribute(cmd, 'action') || resource_name(cmd)).to_s
     condition = Nokogiri::XML(cmd.to_xml).xpath('//ident[@value="only_if" or @value="not_if" or @value="creates"][parent::fcall or parent::command or ancestor::if]')
     (condition.empty? && !cmd_actions.include?("nothing"))
   end.map{|cmd| match(cmd)}
 end
end


## Execute resource used to run chef-provided command(tag: correctness)

@corecommands = ["yum -y", "yum install", "yum reinstall", "yum remove", "mkdir", "useradd", "usermod", "touch"]
rule "AIG_RFC005", "Execute resource used to run chef-provided command" do
 tags %w{style recipe}
 recipe do |ast|
   find_resources(ast, :type => 'execute').find_all do |cmd|
     cmd_str = (resource_attribute(cmd, 'command') || resource_name(cmd)).to_s
     @corecommands.any? { |corecommand| cmd_str.include? corecommand }
   end.map{|c| match(c)}
 end
end


## Templates and CookbookFiles must include # File managed by Chef Cookbook - #{cookbook_name}

rule 'AIG_RFC006', 'Templates and CookbookFiles must include # File managed by Chef Cookbook - #{cookbook_name}' do
  tags %w{style recipe}
  cookbook do |path|
    files  = Dir["#{path}/templates/**/*"]
    files += Dir["#{path}/files/default/**/*"]
    files.collect do |file|
      lines = File.readlines(file)
      lines.collect.with_index do |line, index|
        if line.match("(File)\s+(managed)\s+(by)\s+(Chef)\s+(Cookbook)\s+-\s+#{cookbook}")
          {
            :filename => file,
            :matched => file,
            :line => index+1,
            :column => 0
          }
        end
      end.compact
    end.flatten
  end
end

## check for recips which has '-' in it's names

rule 'AIG-RFC007', 'recipe name should not contain -' do
  tags %w{style recipe}
  cookbook do |path|
    recipes  = Dir["#{path}/{#{standard_cookbook_subdirs.join(',')}}/**/*.rb"]
    recipes += Dir["#{path}/*.rb"]
    recipes.collect do |recipe|
        if recipe.match('(.*)(-)(.*)')
          {
            :filename => recipe,
            :matched => recipe,
          }
        end
      end.compact
    end
  end
