------



### AIG-UFC001: Check if smoke test files exist for each recipes

Check if smoke test files exist for each recipes.

This warning is shown if a recipe does not include a smoke test. A smoke test allows us to check the actual files existed or not.


__recipe without smoke test written__

This example would match the AIG_SFC001 rule because the recipe is not having this unit tests .

```ruby
##### Don't do this
cookbook/recipes/rhel.rb
cookbook/test/smoke/controls/rhel_spec.rb --> missing smoke test for recipe rhel.rb file
end
```

__recipe with smoke test written__

This modified example would not match the AIG_SFC001 rule:


```ruby
##### Use this
cookbook/recipes/rhel.rb
cookbook/test/smoke/controls/rhel_spec.rb
end
```