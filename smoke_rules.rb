
#
# Foodcritic:: StandardPractices
# RuleSet:: Smoke
#
# maintainer:: Opttech Corp, LLC
# maintainer_email:: devops@opttechcorp.com
#
# Copyright:: 2017, Opttech Corp, All Rights Reserved.


## check if there is valid smoke test files exist or not

rule "AIG_SFC001", "Include a Smoke test for every recipe" do
  tags %w{testing lookout}
  cookbook do |cookbook_path|
    matches = []
    recipes = Dir.glob(File.join(cookbook_path, 'recipes/*.rb'))

    recipes.each do |r|
      recipe_name = File.basename(r, '.rb')
      valid_smoke_paths = [
        "test/smoke/#{recipe}/controls/#{recipe_name}_spec.rb",
      ]
      smoke_exist = valid_smoke_paths.any? do |p|
        recipe_smoke = File.join(cookbook_path, p)
        File.exist?(recipe_smoke)
      end

      unless smoke_exist
        preferred_smoke_path = File.join(cookbook_path, valid_smoke_paths[0])
        matches << file_match(preferred_smoke_path)
      end
    end

    matches
  end
end
