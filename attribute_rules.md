
----------
### AIG-AFC001: Password declaration check in attribute file.(tag: correctness)


Check for the password declaration in attribute file.

__attributes with password declaration__

This example would match the AIG-AFC001 rule because password is defined as attribute

```ruby
##### Don't do this
default[aig_example][password] = 'password'
end
```

__recipe with smoke test written__

This modified example would not match the AIG_SFC001 rule:


```ruby
##### Use this
put all your passwords in HASH Valut 
end

----------
