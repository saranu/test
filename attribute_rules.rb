#
# Foodcritic:: StandardPractices
# RuleSet:: Attribute
#
# maintainer:: Opttech Corp, LLC
# maintainer_email:: devops@opttechcorp.com
#
# Copyright:: 2017, Opttech Corp, All Rights Reserved.

## password declaration check in attribute file : - This rule need to check if there is any plane text password declaration in attribute files. (tag:attributes)

rule 'AIG-AFC001', 'password declaration check in attribute file' do
  tags %w{bug cink}
  cookbook do |path|
    attributes  = Dir["#{path}/attributes/**/*.rb"]
    attributes += Dir["#{path}/*.rb"]
    attributes.collect do |attribute|
      lines = File.readlines(attribute)

      lines.collect.with_index do |line, index|
        if line.match('(.*)(password)(.*)')
          {
            :filename => recipe,
            :matched => recipe,
            :line => index+1,
            :column => 0
          }
        end
      end.compact
    end.flatten
  end
end