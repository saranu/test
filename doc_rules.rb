#
# Foodcritic:: StandardPractices
# RuleSet:: Documentation
#
# maintainer:: Opttech Corp, LLC
# maintainer_email:: devops@opttechcorp.com
#
# Copyright:: 2017, Opttech Corp, All Rights Reserved.

## rule for to check if CHANGELOG.md in markdown format exist or not.(tag: style)

rule 'AIG_DOC001', 'Missing CHANGELOG in markdown format' do
 tags %w(style changelog sous-chefs)
 cookbook do |path|
   ensure_file_exists path, 'CHANGELOG.md'
 end
end

## rule for to check if LICENSE file exist or not. (tag: style)

rule 'AIG_DOC002', 'Missing LICENSE in markdown format' do
 tags %w(style changelog sous-chefs)
 cookbook do |path|
   ensure_file_exists path, 'LICENSE'
 end
end