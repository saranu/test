----

### AIG_SFC001: Check if spec test files exist for each recipes


Check if spec test files exist for each recipes.
```
This rule has been deprecated.It will helps us in Check if spec test files exist for each recipes
```


This rule has been deprecated.It will helps us in Check if spec test files exist for each recipes


__recipe without spec test written__

This example would match the AIG_SFC001 rule because the recipe is not having thess spec tests .

```ruby
##### Don't do this
cookbook/recipes/rhel.rb
cookbook/spec/unit/recipes/ --> missing spec test for rhel_spec.rb file
end
```

__recipe with spec test written__

This modified example would not match the AIG_SFC001 rule:


```ruby
##### Use this
cookbook/recipes/rhel.rb
cookbook/spec/unit/recipes//rhel_spec.rb
end

```
----------
