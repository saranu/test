### AIG_DOC001- Check if __CHANGELOG.md__ in markdown format exist or not.(tag: style)

This rule detects if CHANGELOG.md in markdown format exist or not.(tag: style)*. This warning will be shown if you failed to have the CHANGELOG.md.



``` style```      ``` CHANGELOG.md ```                                                                                         


```              																																								
# Create a file CHANGELOG.md with all the updated history

```																												   



---


### AIG_DOC002 - Check if LICENSE in markdown format exist or not.(tag: style)

This warning is shown if a cookbook does not include a LICENSE file. A LICENSE file allows consumers of cookbooks to determine if the terms allow them to use, change or distribute a cookbook. 

``` style```      ``` license ```                                                                                          



```              																																								
# create a LICENSE with all the appropriate information

```				






