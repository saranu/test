----------
### AIG_RFC001 - Resource check with no action. missing default action check.(tag: correctness)

Check if Resource is having the default action or not.

__Resource with no action__

This example would match the AIG_RFC001 rule because the action of the resource is missing action.

```ruby
##### Don't do this
package node[:cookbook][:package] do
end
```
__Resource with action__

 This modified example would not match the AIG_RFC001 :thumbsup: This resource looks great.

```ruby
 ##### Use This
 package node[:cookbook][:package] do
   action :install
 end
 ```
----------

### AIG_RFC002 -  Password declaration check in recipe files.(tag: correctness)

Check for the password declaration in recipe files.

__Recipes with password declaration__

This example would match the AIG_RFC002 rule because password declaration in Recipe file.

```ruby
##### Don't do this
password = 'abc123!@#'
```
__Recipe with password fix__

 This modified example would not match the AIG_RFC002 :thumbsup: This resource looks great.

```ruby
 ##### Use This
 please use hashivault
```

----------

### AIG_RFC003: Execute resource defined without conditional or action :nothing(tag: correctness)

This warning is shown if the resource is defined without a condition/action:nothing. You can read more about the service resource here:

https://docs.chef.io/resource_service.html

__Resource without condition__

This example would match the AIG_RFC003 rule because the Resource without condition/action is missing.

```ruby
##### Don't do this
execute 'enable ssh' do
  command '/usr/sbin/systemsetup -setremotelogin on'
  action :run
end
```
__Resource with condition__

This modified example would not match the AIG_RFC003 rule: It will works great :thumbsup:

```ruby
# Use this
execute 'enable ssh' do
  command '/usr/sbin/systemsetup -setremotelogin on'
  not_if '/usr/sbin/systemsetup -getremotelogin | /usr/bin/grep On'
  action :run
end

```
----------

### AIG_RFC004: bash resource defined without conditional or action :nothing(tag: correctness)


This warning is shown if the resource is defined without a condition/action:nothing. You can read more about the service resource here:

https://docs.chef.io/resource_service.html

__Resource without condition__

This example would match the AIG_RFC004 rule because the Resource without condition/action is missing.

```ruby
##### Don't do this
bash 'extract_module' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{extract_path}
    tar xzf #{src_filename} -C #{extract_path}
    mv #{extract_path}/*/* #{extract_path}/
    EOH
end
```


__Resource with condition__

This modified example would not match the AIG_RFC004 rule: It will works great :thumbsup:


```ruby
# Use this
bash 'extract_module' do
  cwd ::File.dirname(src_filepath)
  code <<-EOH
    mkdir -p #{extract_path}
    tar xzf #{src_filename} -C #{extract_path}
    mv #{extract_path}/*/* #{extract_path}/
    EOH
  not_if { ::File.exist?(extract_path) }
end

```

----------

### AIG_RFC005: Execute resource used to run chef-provided command(tag: correctness)


This warning will be shown when the Execute resource is not running with chef-provided command.

__Resource without using chef-provided command__

This example would match the AIG_RFC005 rule because the resource is not running with chef-provided command.:-1:

```ruby
##### Don't do this
execute "reinstall_pear" do
  command "yum -y install php-pear"
end
```

__Resource with Chef-provided command__

This modified example would not match the AIG_RFC005 rule: This will looks great :thumbsup:


```ruby
# Use this
package 'php-pear' do
  action :install
end

```

----------


### AIG_RFC006: Templates and CookbookFiles must include # File managed by Chef Cookbook - #{cookbook_name}

This rule will make sure the templates and CookbookFiles will be under the cookbook control.


This example would match the AIG_RFC006 rule because the resource is not running with chef-provided command.

```ruby
##### Don't do this
vi cookbookname/files/httpd.conf missing below commented line if httpd.conf is managed by chef
#File managed by Chef Cookbook - #{cookbook_name}
#
```

This modified example would not match the AIG_RFC006 rule: This will looks great :thumbsup:

```ruby
# Use this
vi cookbookname/files/httpd.conf add below comment
#File managed by Chef Cookbook - #{cookbook_name}

```

----------
### AIG_RFC007: check for recipe name with '-'(tag: correctness)


__Recipe  with using '-' in name__

This example would match the AIG_RFC007 rule because recipe name includes '-'

```ruby
##### Don't do this
cookbookname/aig-example.rb
```

__Recipe without using  '-' in name__

This modified example would not match the AIG_RFC007 rule: This will looks great :thumbsup:

```ruby
# Use this
cookbookname/aig_example.rb

```
----------
